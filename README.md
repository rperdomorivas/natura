Buenas. Soy Ramón Perdomo y esto es un repositorio donde guardo todo lo relativo
al desafío propuesto por Natura.

En este desafío decidí agregar un botón cuya función sea llevarnos al principio
o cima de una página web.

Usé una enorme página de Wikipedia porque fue esa carencia en esa página la que
me impulsó a tomar esto como desafío.

Uso este repositorio porque será mucho más fácil para ustedes encontrar las
líneas que contienen los cambios que hice.

Sólo modifiqué el .html, agregué un archivo .css, una imagen para el botón y
dos imágenes del antes y después de realizar el desafío.

Desde ya, muchas gracias por dejarme participar! '-')/